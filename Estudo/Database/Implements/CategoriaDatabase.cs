﻿using Estudo.Database.Abstract;
using Estudo.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Database.Implements
{
    class CategoriaDatabase : BaseDB, ICrud<tb_categoria>
    {
        public void Salvar(Entity.tb_categoria model)
        {
            _db.tb_categoria.Add(model);
            _db.SaveChanges();
        }

        public void Remover(int id)
        {
            Entity.tb_categoria rem =
                _db.tb_categoria.FirstOrDefault(x => x.id_categoria == id);
            _db.tb_categoria.Remove(rem);
            _db.SaveChanges();
        }

        public void Alterar(Entity.tb_categoria model)
        {
            Entity.tb_categoria alt = 
                _db.tb_categoria.FirstOrDefault(x => x.id_categoria == model.id_categoria);

            alt.nm_categoria = model.nm_categoria;
            _db.SaveChanges();
        }

        public Entity.tb_categoria Consultar(int id)
        {
            Entity.tb_categoria categoria =
                _db.tb_categoria.FirstOrDefault(x => x.id_categoria == id);
            return categoria;

        }

        public List<Entity.tb_categoria> ListarTodos()
        {
            List<Entity.tb_categoria> categorias =
                _db.tb_categoria.ToList();
            return categorias;
        }

    }
}
