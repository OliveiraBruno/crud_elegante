﻿using Estudo.Database.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Database.Implements
{
    class ProdutoDatabase : BaseDB, ICrudProduto
    {
        public void Salvar(Entity.tb_produto model)
        {
            _db.tb_produto.Add(model);
            _db.SaveChanges();
        }

        public void Remover(int id)
        {
            Entity.tb_produto rem =
                _db.tb_produto.FirstOrDefault(x => x.id_produto == id);
            _db.tb_produto.Remove(rem);
            _db.SaveChanges();
        }

        public void Alterar(Entity.tb_produto model)
        {
            Entity.tb_produto alt =
                _db.tb_produto.FirstOrDefault(x => x.id_produto == model.id_produto);

            alt.nm_produto = model.nm_produto;
            alt.vl_preco = model.vl_preco;
            alt.id_categoria = model.id_categoria;
            alt.img_produto = model.img_produto;
            _db.SaveChanges();
        }

        public Entity.tb_produto Consultar(int id)
        {
            Entity.tb_produto produto =
                _db.tb_produto.FirstOrDefault(x => x.id_produto == id);
            return produto;

        }

        public List<Entity.tb_produto> Consultar(string produto, string categoria)
        {
            List<Entity.tb_produto> produtos =
                _db.tb_produto.Where(x => x.nm_produto.Contains(produto)
                                      && x.tb_categoria.nm_categoria.Contains(categoria))
                             .ToList();
            return produtos;
        }
        

        public List<Entity.tb_produto> ListarTodos()
        {
            List<Entity.tb_produto> produtos =
                _db.tb_produto.ToList();
            return produtos;
        }

    }
}
