﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Database.Abstract
{
    interface ICrudProduto : ICrud<Entity.tb_produto>
    {
        List<Entity.tb_produto> Consultar(string produto, string categoria);
    }
}
