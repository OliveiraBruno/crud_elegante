﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Database.Abstract
{
    interface ICrud<T>
    {
        void Salvar(T model);
        void Alterar(T model);
        void Remover(int id);
        T Consultar(int id);
        List<T> ListarTodos();
    }
}
