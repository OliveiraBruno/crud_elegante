﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Estudo.Utils;

namespace Estudo
{
    public partial class frmCrud : Form
    {
        Business.CategoriaBusiness _categoriaBusiness = new Business.CategoriaBusiness(new Database.Implements.CategoriaDatabase());
        Business.ProdutoBusiness _produtoBusiness = new Business.ProdutoBusiness(new Database.Implements.ProdutoDatabase());

        Database.Entity.tb_produto _produto;
        bool _alterando = false;

        public frmCrud()
        {
            InitializeComponent();
            this.CarregarCombo();
            this.CarregarGrid();
        }


        #region | Carregamento Inicial   |

        private void CarregarCombo()
        {
            cboCategorias.DisplayMember = nameof(Database.Entity.tb_categoria.nm_categoria);
            cboCategorias.ValueMember = nameof(Database.Entity.tb_categoria.id_categoria);

            cboCategorias.DataSource = _categoriaBusiness.ListarTodos();
        }

        private void CarregarGrid()
        {
            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = _produtoBusiness.ListarTodos();
        }

        #endregion

        #region | Inserir e Alterar      |
        
        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (_alterando == false)
                    _produto = new Database.Entity.tb_produto();

                var categoria = cboCategorias.SelectedItem as Database.Entity.tb_categoria;
                _produto.nm_produto = txtNome.Text;
                _produto.vl_preco   = txtPreco.Text.ToDecimal();
                _produto.id_categoria = categoria.id_categoria;
                _produto.img_produto = this.LerImagem();

                _produtoBusiness.Salvar(_produto);

                MessageBox.Show("Salvo com sucesso.", "Vibranium", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.LimparCampos();
                this.CarregarGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Vibranium", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Utils.LogError.Log(ex);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro inesperado. Entre em contato com o administrador do sistema.", "Vibranium", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.LogError.Log(ex);
            }   
        }

        private void LimparCampos()
        {
            _alterando = false;
            _produto = null;
            txtId.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtPreco.Text = string.Empty;
            imgProduto.Image = Properties.Resources.tk2;
        }

        private byte[] LerImagem()
        {
            if (imgProduto.Image == null)
                return null;

            using (var ms = new System.IO.MemoryStream())
            {
                imgProduto.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }

        private void ImgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            DialogResult resposta = janela.ShowDialog();

            if (resposta == DialogResult.OK)
            {
                imgProduto.ImageLocation = janela.FileName;
            }
        }

        #endregion

        #region | Eventos Grid           |

        private void DgvProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var produto = dgvProdutos.CurrentRow.DataBoundItem as Database.Entity.tb_produto;

            if (e.ColumnIndex == 4)
            {
                this.EditarProduto(produto);
            }

            if (e.ColumnIndex == 5)
            {
                this.RemoverProduto(produto);   
            }
        }

        private void EditarProduto(Database.Entity.tb_produto produto)
        {
            _alterando = true;
            _produto = produto;
            txtId.Text = produto.id_produto.ToString();
            txtNome.Text = produto.nm_produto;
            txtPreco.Text = produto.vl_preco.ToString();
            cboCategorias.SelectedValue = produto.id_categoria;
            imgProduto.Image = CarregarImagem(produto.img_produto);
        }

        private void RemoverProduto(Database.Entity.tb_produto produto)
        {
            DialogResult resposta =
                    MessageBox.Show("Deseja remover o produto?",
                                    "Vibranium",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

            if (resposta == DialogResult.Yes)
            {
                _produtoBusiness.Remover(produto.id_produto);
                this.CarregarGrid();
            }
        }

        private Image CarregarImagem(byte[] imagem)
        {
            if (imagem == null)
                return null;

            using (var ms = new System.IO.MemoryStream(imagem))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }

        #endregion

        #region | Buscar                 |

        private void TxtBuscarNome_TextChanged(object sender, EventArgs e)
        {
            this.BuscarProdutos();
        }

        private void TxtBuscarCategoria_TextChanged(object sender, EventArgs e)
        {
            this.BuscarProdutos();
        }

        private void BuscarProdutos()
        {
            string produto = txtBuscarNome.Text;
            string categoria = txtBuscarCategoria.Text;

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = _produtoBusiness.Consultar(produto, categoria);
        }


        #endregion

        
    }
}
