﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Utils
{
    static class ConvertionExtensions
    {
        public static decimal ToDecimal(this string valor)
        {
            decimal numero = 0;

            if (!decimal.TryParse(valor, out numero))
                numero = 0;

            return numero;
        }
    }
}
