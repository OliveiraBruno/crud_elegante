﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Utils
{
    class Validador
    {
        public bool Validado { get; set; }
        public string Mensagem { get; set; }


        public Validador()
        {
            Validado = true;
        }

        public Validador CampoObrigatorio(string valor)
        {
            if (valor == null || valor == string.Empty)
            {
                Mensagem = "Preencha os campos obrigatórios.";
                Validado = false;
            }
            return this;
        }

        public Validador CampoObrigatorio(decimal valor)
        {
            if (valor == 0)
            {
                Mensagem = "Preencha os campos obrigatórios.";
                Validado = false;
            }
            return this;
        }

        public Validador Positivo(decimal valor)
        {
            if (valor <= 0)
            {
                Mensagem = "Preencha os campos com valores positivos.";
                Validado = false;
            }
            return this;
        }

        public Validador MaximoCaracteres(string valor, int max)
        {
            if (valor.Length > max)
            {
                Mensagem = "O máximo de caracteres foi excedido.";
                Validado = false;
            }
            return this;
        }

        public Validador MinimoCaracteres(string valor, int min)
        {
            if (valor.Length < min)
            {
                Mensagem = "O mínimo de caracteres não foi atingido.";
                Validado = false;
            }
            return this;
        }
    }
}
