﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Utils
{
    static class LogError
    {
        const string NOME_ARQUIVO = "log.txt";

        public static void Log(Exception ex)
        {
            using (var arquivo = new System.IO.StreamWriter(NOME_ARQUIVO, true))
            {
                arquivo.WriteLine($"{DateTime.Now} \t {ex} \n\n");
            }
        }
    }
}
