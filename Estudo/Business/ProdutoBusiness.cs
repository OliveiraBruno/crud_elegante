﻿using Estudo.Database.Abstract;
using Estudo.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Business
{
    class ProdutoBusiness
    {
        ICrudProduto _db;
        public ProdutoBusiness(ICrudProduto db)
        {
            _db = db;
        }


        public void Salvar(tb_produto produto)
        {
            if (produto.id_produto == 0)
                this.Cadastrar(produto);
            else
                this.Alterar(produto);
        }

        private void Cadastrar(tb_produto produto)
        {
            this.ValidarModelo(produto);
            _db.Salvar(produto);
        }


        private void Alterar(tb_produto produto)
        {
            this.ValidarModelo(produto);
            this.ValidarProdutoExiste(produto.id_produto);
            _db.Alterar(produto);
        }


        public void Remover(int id)
        {
            this.ValidarProdutoExiste(id);   
            _db.Remover(id);
        }


        public tb_produto Consultar(int id)
        {
            tb_produto produto =
                _db.Consultar(id);
            return produto;

        }


        public List<tb_produto> Consultar(string produto, string categoria)
        {
            List<tb_produto> produtos =
                _db.Consultar(produto, categoria);
            return produtos;
        }


        public List<tb_produto> ListarTodos()
        {
            List<tb_produto> produtos =
                _db.ListarTodos();
            return produtos;
        }

        



        private void ValidarProdutoExiste(int id)
        {
            var produto = this.Consultar(id);
            if (produto == null)
                throw new ArgumentException("Produto não encontrado.");
        }

        
        private void ValidarModelo(tb_produto produto)
        {
            Utils.Validador validador = new Utils.Validador();
            if (!validador.CampoObrigatorio(produto.nm_produto)
                          .MaximoCaracteres(produto.nm_produto, 45)
                          .MinimoCaracteres(produto.nm_produto, 5)
                          .CampoObrigatorio(produto.vl_preco)
                          .Positivo(produto.vl_preco)
                          .CampoObrigatorio(produto.id_categoria)
                          .Validado)
                throw new ArgumentException(validador.Mensagem);

            this.ValidarCategoriaExiste(produto.id_categoria);
        }

        private void ValidarCategoriaExiste(int id)
        {
            var categoriaBusiness = 
                new CategoriaBusiness(new Database.Implements.CategoriaDatabase());

            var categoria = categoriaBusiness.Consultar(id);
            if (categoria == null)
                throw new ArgumentException("Categoria não encontrada.");
        }
    }
}
