﻿using Estudo.Database.Abstract;
using Estudo.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudo.Business
{
    class CategoriaBusiness
    {
        ICrud<tb_categoria> _db;
        public CategoriaBusiness(ICrud<tb_categoria> db)
        {
            _db = db;
        }


        public void Salvar(tb_categoria categoria)
        {
            this.ValidarModelo(categoria);
            _db.Salvar(categoria);
        }


        public List<tb_categoria> ListarTodos()
        {
            var categorias = _db.ListarTodos();
            return categorias;
        }


        public tb_categoria Consultar(int id)
        {
            var categoria = _db.Consultar(id);
            return categoria;
        }


        private void ValidarModelo(tb_categoria categoria)
        {
            Utils.Validador validador = new Utils.Validador();

            if (!validador.CampoObrigatorio(categoria.nm_categoria)
                          .MaximoCaracteres(categoria.nm_categoria, 45)
                          .MinimoCaracteres(categoria.nm_categoria, 5)
                          .Validado)
                throw new ArgumentException(validador.Mensagem);
        }
    }
}
